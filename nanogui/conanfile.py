from conans import ConanFile, CMake, tools

APP_NAME = "nanogui"

class NanoguiConan(ConanFile):
    name = APP_NAME
    version = "latest"
    license = "<Put the package license here>"
    url = "https://github.com/mitsuba-renderer/nanogui"
    description = "Minimalistic C++/Python GUI library for OpenGL, GLES2/3, Metal, and WebAssembly/WebGL"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    _cmake = None

    def source(self):
        self.run("git clone --recursive https://github.com/mitsuba-renderer/nanogui.git")
        #self.run("cd hello && git checkout static_shared")
        # This small hack might be useful to guarantee proper /MT /MD linkage in MSVC
        # if the packaged project doesn't have variables to set it properly
        tools.replace_in_file("nanogui/CMakeLists.txt",
        '''
project(nanogui
  DESCRIPTION
    "NanoGUI"
  LANGUAGES
    CXX C
)''' , '''project(nanogui
  DESCRIPTION
    "NanoGUI"
  LANGUAGES
    CXX C
)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def cmake_configure(self):
        if (self._cmake != None):
            return self._cmake
        self._cmake = CMake(self)
        self._cmake.definitions["NANOGUI_BUILD_PYTHON"] = False
        self._cmake.definitions["NANOGUI_BUILD_EXAMPLE"] = False
        self._cmake.configure(source_folder=APP_NAME)
        return self._cmake

    def build(self):
        cmake = self.cmake_configure()
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s' % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src=APP_NAME)
        self.copy("libnanogui*", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = [APP_NAME]
