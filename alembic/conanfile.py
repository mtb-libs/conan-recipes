import os

from conans import ConanFile, tools, CMake
from conan.tools.cmake import CMakeDeps

class AlembicConan(ConanFile):
    name = "alembic"
    version = "1.8.2"
    author = "Sony Pictures Imageworks | Industrial Light and Magic"
    url = "https://github.com/alembic/alembic"
    description = "Alembic is an open framework for storing and sharing scene data that includes a C++ library, a file format, and client plugins and applications."
    topics = ("cgi", "vfx", "cache")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "python":[True,False] }
    default_options = {"shared": True, "python": True}
    generators = ("CMakeDeps")

    requires = ("boost/1.75.0", "openexr/2.5.7","Imath/3.1.2", "hdf5/1.12.0")
    _source_subfolder = "alembic-source"
    _build_subfolder = "build_subfolder"
    # exports_sources = "CMakeLists.txt"
    _cmake = None
    def source(self):
        tools.get(
            "https://github.com/alembic/alembic/archive/refs/tags/{}.tar.gz".format(
                self.version
            ),
        )
        target_name = "{0}-{1}".format(self.name, self.version)
        os.rename(target_name, self._source_subfolder)

        # tools.replace_in_file(os.path.join(self._source_subfolder,"cmake", "AlembicPyIlmBase.cmake"),
        #                       "SET(ALEMBIC_PYILMBASE_PYIMATH_LIB Imath::PyImath_Python${PYTHON_VERSION_MAJOR}_${PYTHON_VERSION_MINOR})",
        #                       """SET(ALEMBIC_PYILMBASE_PYIMATH_LIB Imath::PyImath_Python)""")

    def config_options(self):
        if self.options.python:
            import importlib
            importlib.import_module("numpy")

            self.options["boost"].without_python = False
        return super().config_options()

    def generate(self):
        cmake = CMakeDeps(self)
        cmake.generate()

    def _configure_cmake(self):
        # os.environ.update(
        #     {"TBB_LOCATION": self.deps_cpp_info["tbb"].rootpath,}
        # )
        if self._cmake:
            return self._cmake
        self._cmake = CMake(self)
        self._cmake.definitions["ALEMBIC_SHARED_LIBS"] = self.options.shared
        self._cmake.definitions["USE_PYALEMBIC"] = self.options.python
        self._cmake.definitions["USE_STATIC_BOOST"] = not self.options.shared
        self._cmake.definitions["PYALEMBIC_PYTHON_MAJOR"] = 3
        self._cmake.definitions["USE_HDF5"] = True

        self._cmake.configure(source_folder=self._source_subfolder,build_folder=self._build_subfolder)
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["alembic"]

