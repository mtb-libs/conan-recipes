from conans import ConanFile, CMake, tools


class ClipConan(ConanFile):
    name = "clip"
    version = "1.3.0"
    license = "MIT"
    author = "David Capello"
    url = "https://github.com/dacap/clip"
    description = "Cross-platform C++ library to copy/paste clipboard content"
    topics = ("ui", "clipboard")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    exports_sources = [("clip/*")]
    requires = []

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
            
    def source(self):
      git = tools.Git(folder="clip")
      git.clone("https://github.com/dacap/clip.git")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="clip")
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.h", dst="include", src="clip")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["clip"]
