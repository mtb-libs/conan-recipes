from conans import ConanFile, AutoToolsBuildEnvironment, tools
import os
import glob
import shutil


class FBXSDKConan(ConanFile):
    name = "fbxsdk"
    url = "https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-archives"
    description = "free, easy-to-use, C++ software development platform and API toolkit that allows application and content vendors to transfer existing content into the FBX format with minimal effort."
    # https://github.com/FFmpeg/FFmpeg/blob/master/LICENSE.md
    homepage = "https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-archives"
    topics = ("fbx", "sdk", "dcc", "format")
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "pkg_config"

    _source_subfolder = "source_subfolder"
    _include_root = ""

    requires = ["libxml2/2.9.10"]

    @property
    def _is_mingw_windows(self):
        return (
            self.settings.os == "Windows"
            and self.settings.compiler == "gcc"
            and os.name == "nt"
        )

    @property
    def _is_msvc(self):
        return self.settings.compiler == "Visual Studio"

    def source(self):
        suffix = ""
        if self.settings.os == "Windows":
            suffix = "win"
        elif self.settings.os == "Macos":
            suffix = "mac"

        version = "{}-{}".format(self.version, suffix)
        tools.get(**self.conan_data["sources"][version])
        extracted_dir = self.name + "-" + self.version
        if self.settings.os == "Macos":
            p = [x for x in os.listdir(".")]
            pk = [x for x in p if x.endswith("pkg")][0]

            if self.major() < 2018:
                arch = self.find("Archive.pax.gz")
                self.run("gunzip -c {} | cpio -i".format(arch))
            else:
                self.run(
                    "pkgutil --expand-full {} {}".format(pk, self._source_subfolder)
                )
        else:
            self.output.error("Error, only macOs is supported for now.")
            os.rename(extracted_dir, self._source_subfolder)

    def major(self):
        return int(self.version.split(".")[0])

    # def configure(self):
    #     del self.settings.compiler.libcxx
    #     del self.settings.compiler.cppstd

    # def config_options(self):
    #     if self.settings.os == "Windows":
    #         del self.options.fPIC
    #     if self.settings.os != "Linux":
    #         self.options.remove("vaapi")
    #         self.options.remove("vdpau")
    #         self.options.remove("xcb")
    #         self.options.remove("alsa")
    #         self.options.remove("pulse")
    #     if self.settings.os != "Macos":
    #         self.options.remove("appkit")
    #         self.options.remove("avfoundation")
    #         self.options.remove("coreimage")
    #         self.options.remove("audiotoolbox")
    #         self.options.remove("videotoolbox")
    #         self.options.remove("securetransport")
    #     if self.settings.os != "Windows":
    #         self.options.remove("qsv")

    # def build_requirements(self):
    #     self.build_requires("yasm/1.3.0")
    #     if tools.os_info.is_windows:
    #         if "CONAN_BASH_PATH" not in os.environ:
    #             self.build_requires("msys2/20200517")
    #     if self.settings.os == "Linux":
    #         if not tools.which("pkg-config"):
    #             self.build_requires("pkgconf/1.7.3")
    @staticmethod
    def find(name, path="."):
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)

    def package(self):
        # self.copy("**/include/fbxsdk.h", dst="include", keep_path=False)
        fbxh = self.find("fbxsdk.h", os.curdir)
        fbxh = os.path.dirname(fbxh)

        self.copy("*.h", src=fbxh, dst="include", keep_path=True)
        # self.copy("*/clang/release/*.lib", dst="lib", keep_path=False)
        # self.copy("*/clang/release/*.dll", dst="bin", keep_path=False)
        if self.settings.os == "Macos":
            if self.major() < 2018:
                if self.options.shared:
                    self.copy("*/clang/**/release/*.dylib*", dst="lib", keep_path=False)
                else:
                    self.copy(
                        "*/clang/**/release/*.a", dst="lib/static", keep_path=False
                    )
            else:
                if self.options.shared:
                    self.copy("*/clang/release/*.dylib*", dst="lib", keep_path=False)
                else:
                    self.copy("*/clang/release/*.a", dst="lib/static", keep_path=False)

    def package_id(self):
        del self.info.options.shared

    def package_info(self):  # still very useful for package consumers
        self.cpp_info.libs = ["fbxsdk"]
        # self.cpp_info.libs = self.collect_libs()
        self.cpp_info.cxxflags = ["-stdlib=libc++"]
        self.cpp_info.exelinkflags = [
            "-stdlib=libc++",
        ]
        self.cpp_info.frameworks.extend(["Carbon", "Cocoa", "SystemConfiguration"])
        if self.options.shared:
            self.cpp_info.defines = ["DFBXSDK_SHARED"]
        else:
            self.cpp_info.libdirs = ["lib/static"]
