# Conan Recipes

Those are recipes I mostly gathered (from the to be defunct [Bintray](https://bintray.com/) and [Conan Center](https://conan.io/center/)) + some custom ones for missing recipes. Those are automatically deployed on [MTB](https://mtb.jfrog.io/) (Mel's ToolBox)

## working recipes
- [clip](https://github.com/dacap/clip) - Cross-platform C++ library to copy/paste clipboard content
- [embree3](https://github.com/embree/embree) - Intel's Ray tracing kernels
- [ffmpeg](https://github.com/FFmpeg/FFmpeg) - collection of libraries and tools to process multimedia content such as audio, video, subtitles and related metadata.
- [filament](https://github.com/google/filament) - Filament is a real-time physically based rendering engine for Android, iOS, Windows, Linux, macOS, and WebGL2
- [fluidsynth](https://github.com/FluidSynth/fluidsynth) - Software synthesizer based on the SoundFont 2 specifications
- *[DEPRECATED]* [ilmbase](https://github.com/AcademySoftwareFoundation/openexr) - OpenEXR ILM Base libraries 
- [imagemagick](https://github.com/ImageMagick/ImageMagick) - Extensive mage manipulation tool set.
- imgui
- [juce](https://github.com/juce-framework/JUCE) - JUCE is an open-source cross-platform C++ application framework for desktop and mobile applications, including VST, VST3, AU, AUv3, RTAS and AAX audio plug-ins.
- [libremidi](https://github.com/jcelerier/libremidi) - A modern C++ MIDI real-time & file I/O library. Supports Windows, macOS, Linux and WebMIDI.
- [opensubdiv](https://github.com/PixarAnimationStudios/OpenSubdiv) - Pixar's Open-Source subdivision surface library.
- [sdl2](https://github.com/libsdl-org/SDL) - Simple Directmedia Layer
- [sokol](https://github.com/floooh/sokol) - minimal cross-platform standalone C headers
- [usd](https://github.com/PixarAnimationStudios/USD) - Pixar's Universal Scene Description