from conans import ConanFile, CMake, tools


class LibremidiConan(ConanFile):
    name = "JUCE"
    version = "6.0.8"
    # None of the forked project seems to have proper licenses
    license = "juce-6-licence"
    author = "JUCE info@juce.com"
    url = "https://github.com/juce-framework/JUCE"
    description = "JUCE is an open-source cross-platform C++ application framework for desktop and mobile applications, including VST, VST3, AU, AUv3, RTAS and AAX audio plug-ins."
    topics = (
        "audio",
        "plugin",
        "c-plus-plus",
        "framework",
        "cpp",
        "vst",
        "au",
        "vst3",
        "juce",
        "aax",
        "audiounit",
        "auv3",
    )
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "build_examples": [True, False],
        "build_extras": [True, False],
        "enable_module_source_groups": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "build_examples": False,
        "build_extras": False,
        "enable_module_source_groups": False,
    }
    generators = "cmake"

    def source(self):
        git = tools.Git(folder="JUCE")
        git.clone(self.url + ".git", self.version)

        # Not needed here

    #         tools.replace_in_file(
    #             "JUCE/CMakeLists.txt",
    #             "project(JUCE VERSION 6.0.8 LANGUAGES C CXX)",
    #             """project(JUCE VERSION 6.0.8 LANGUAGES C CXX)
    # include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    # conan_basic_setup()
    #         """,
    #         )

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _configure_cmake(self):
        cmake = CMake(self)

        cmake.definitions["JUCE_COPY_PLUGIN_AFTER_BUILD"] = True
        cmake.definitions["JUCE_BUILD_EXTRAS"] = self.options.build_extras
        cmake.definitions["JUCE_BUILD_EXAMPLES"] = self.options.build_examples
        cmake.definitions[
            "JUCE_ENABLE_MODULE_SOURCE_GROUPS"
        ] = self.options.enable_module_source_groups

        cmake.configure(source_dir="JUCE")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        # self.copy("*.h", dst="include",src="JUCE/modules", keep_path=True)
        # self.copy("*.app", dst="bin", keep_path=False)
        # self.copy("*.lib", dst="lib", keep_path=False)
        # self.copy("*.dll", dst="bin", keep_path=False)
        # self.copy("*.dylib*", dst="lib", keep_path=False)
        # self.copy("*.so", dst="lib", keep_path=False)
        # self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.build_modules.append("lib/cmake/JUCE-6.0.8/JUCEUtils.cmake")
        self.cpp_info.build_modules.append("lib/cmake/JUCE-6.0.8/JUCEConfig.cmake")
        self.cpp_info.libs = tools.collect_libs(self)
        # [
        #     "juce",
        #     "juce_analytics",
        #     "juce_audio_basics",
        #     "juce_audio_devices",
        #     "juce_audio_formats",
        #     "juce_audio_plugin_client",
        #     "juce_audio_processors",
        #     "juce_audio_utils",
        #     "juce_blocks_basics",
        #     "juce_box2d",
        #     "juce_core",
        #     "juce_cryptography",
        #     "juce_data_structures",
        #     "juce_dsp",
        #     "juce_events",
        #     "juce_graphics",
        #     "juce_gui_basics",
        #     "juce_gui_extra",
        #     "juce_opengl",
        #     "juce_osc",
        #     "juce_product_unlocking",
        #     "juce_video",
        # ]
     
        # if self.settings.os == "Macos":
        #     self.cpp_info.frameworks.extend(["CoreMIDI", "CoreFoundation", "CoreAudio"])
