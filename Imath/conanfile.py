import os

from conans import ConanFile, tools, CMake
from conan.tools.cmake import CMakeDeps
from pathlib import Path

class AlembicConan(ConanFile):
    name = "Imath"
    version = "3.1.2"
    author = "Sony Pictures Imageworks | Industrial Light and Magic"
    url = "https://github.com/AcademySoftwareFoundation/Imath"
    description = "Imath is a C++ and python library of 2D and 3D vector, matrix, and math operations for computer graphics"
    topics = ("cgi", "vfx", "cache")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "python_bindings": [True, False] }
    default_options = {"shared": False, "python_bindings": True}
    generators = ("CMakeDeps")

    requires = ()
    _source_subfolder = "lmath-source"
    _build_subfolder = "build_subfolder"
    # exports_sources = "CMakeLists.txt"
    _cmake = None

    def generate(self):
        cmake = CMakeDeps(self)
        cmake.generate()

    def source(self):
        tools.get(
            "https://github.com/AcademySoftwareFoundation/Imath/archive/refs/tags/v{}.tar.gz".format(
                self.version
            ),
        )
        target_name = "{0}-{1}".format(self.name, self.version)
        os.rename(target_name, self._source_subfolder)
        tools.replace_in_file(os.path.join(self._source_subfolder,"src","python","CMakeLists.txt"),"""python${Python_VERSION_MAJOR}
  ${PYIMATH_BOOST_PY_COMPONENT}"""
        ,"")
        tools.replace_in_file(os.path.join(self._source_subfolder,"src","python","CMakeLists.txt"),"""elseif(NOT _pyimath_have_perver_boost)
  message(WARNING "Unable to find boost::python library, disabling PyImath. If you believe this is wrong, check the cmake documentation and see if you need to set Boost_ROOT or Boost_NO_BOOST_CMAKE")
  return()""","""elseif(NOT _pyimath_have_perver_boost)
  set(PYIMATH_BOOST_PY_COMPONENT python)""")

    def requirements(self):
      if self.options.python_bindings:
        self.requires("boost/1.75.0")

    def config_options(self):
      if self.options.python_bindings:
        import importlib
        importlib.import_module("numpy")

        self.options["boost"].without_python = False
      return super().config_options()
    def _configure_cmake(self):
        # os.environ.update(
        #     {"TBB_LOCATION": self.deps_cpp_info["tbb"].rootpath,}
        # )
        if self._cmake:
            return self._cmake
        self._cmake = CMake(self)
        self._cmake.definitions["Boost_NO_BOOST_CMAKE"] = False
        self._cmake.definitions["PYTHON"] = self.options.python_bindings
        self._cmake.definitions["IMATH_LIB_SUFFIX"] = ""
        self._cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared


        self._cmake.configure(source_folder=self._source_subfolder,build_folder=self._build_subfolder)
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()



    def package(self):
        cmake = self._configure_cmake()
        cmake.install()
        # self.copy(pattern="*.a", dst="lib",keep_path=False)
        # self.copy(pattern="*.h", dst="include",keep_path=False)
        # self.copy(pattern="*.dylib", dst="lib",keep_path=False)
        # self.copy(pattern="*.so", dst="lib",keep_path=False)

        # I don't know why some lib use Imath/XXX.h
        # whereas other use XXX.h directly, majors seems to use the latter
        # install_dir = Path(self.package_folder) / "include" / "Imath"
        # dest_dir = Path(self.package_folder) / "include"
        # self.output.warn("I don't know why some lib use Imath/XXX.h whereas other use XXX.h directly, majors seems to use the latter and this recipe too")
        # for x in install_dir.iterdir():
        #     self.output.rewrite_line("Moving header {}".format(x.name))
        #     x.replace(dest_dir / x.name)

        # self.output.success("Moved all headers to include/")

    def package_info(self):
        self.cpp_info.libs = self.collect_libs()
