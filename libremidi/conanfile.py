from conans import ConanFile, CMake, tools


class LibremidiConan(ConanFile):
    name = "libremidi"
    version = "latest"
    # None of the forked project seems to have proper licenses
    license = "Unlicensed"
    author = "Jean-Michaël Celerier jeanmichael.celerier@gmail.com"
    url = "https://github.com/jcelerier/libremidi"
    description = "A modern C++ MIDI real-time & file I/O library. Supports Windows, macOS, Linux and WebMIDI."
    topics = (
        "midi",
        "webmidi",
        "uwp",
        "emscripten",
        "cpp17",
        "alsa",
        "jack",
        "coremidi",
        "jackaudio",
    )
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "header_only": [True, False],
        "fPIC": [True, False],
        "build_examples": [True, False],
        "disable_coremidi": [True, False],
        "disable_alsa": [True, False],
        "disable_jack": [True, False],
        "disable_winmm": [True, False],
        "disable_winuwp": [True, False],
    }
    default_options = {
        "shared": False,
        "header_only": False,
        "fPIC": True,
        "build_examples": True,
        "disable_coremidi": False,
        "disable_alsa": False,
        "disable_jack": False,
        "disable_winmm": False,
        "disable_winuwp": True
    }
    generators = "cmake"

    def source(self):
        git = tools.Git(folder="libremidi")
        git.clone("https://github.com/jcelerier/libremidi.git")

        # Not needed here
        tools.replace_in_file("libremidi/CMakeLists.txt","project(libremidi CXX)",
        '''project(libremidi CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
        ''')
        tools.replace_in_file("libremidi/CMakeLists.txt","if(TARGET Catch2::Catch2 AND Catch2_FOUND)",
        '''if(TARGET Catch2::Catch2 AND Catch2_FOUND AND BLOUBLABLOUBLI)''')

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def _configure_cmake(self):
        cmake = CMake(self)

        cmake.definitions["LIBREMIDI_HEADER_ONLY"] = self.options.header_only
        cmake.definitions["LIBREMIDI_EXAMPLES"] = self.options.build_examples
        cmake.definitions["LIBREMIDI_NO_COREMIDI"] = self.options.disable_coremidi
        cmake.definitions["LIBREMIDI_NO_ALSA"] = self.options.disable_alsa
        cmake.definitions["LIBREMIDI_NO_JACK"] = self.options.disable_jack
        cmake.definitions["LIBREMIDI_NO_WINMM"] = self.options.disable_winmm
        cmake.definitions["LIBREMIDI_NO_WUNUWP"] = self.options.disable_winuwp

        cmake.configure(source_dir="libremidi")
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("*.hpp", dst="include", src="libremidi/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["libremidi"]
        if self.settings.os == "Macos":
            self.cpp_info.frameworks.extend(["CoreMIDI","CoreFoundation", "CoreAudio"])
