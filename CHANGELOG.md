## Unreleased

### Fix
- 💄 Better options for imgui

### Feat
- 🎨 Add embree3/3.12.2
- 🎨 Add Sokol
- 🎨 Add SDL2
- 🎨 New recipes
- 🎨 Add old recipes
- 🎨 Added Filament (1.9.18)
- 🎨 Added Clip (1.3.0)
- 🎨 Added ImGUI
